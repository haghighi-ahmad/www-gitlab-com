---
layout: handbook-page-toc
title: Jobs - Frequently Asked Questions
description: Browse job openings at GitLab and apply to join a very productive, ambitious team, where independence and flexibility are both valued and required.
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What's it like to work at GitLab?

GitLab's [100% remote culture](/company/culture/all-remote/) and our workplace methodologies are highly unique. You should not expect to transfer the norms of colocated corporations into a work from anywhere scenario. Those who thrive at GitLab take the opportunity to drop prior workplace baggage at the door, embrace a liberating and empowering [set of values](/handbook/values/), and **give themselves permission to truly operate differently**.

So differently, in fact, that many of GitLab's most effective [processes](/company/culture/all-remote/management/) would be discouraged or forbidden in conventional corporations. **It's not a trap**. It's the future of work.

Explore the resources below for a deeper understanding of [life at GitLab](/company/culture/#life-at-gitlab) — the world's largest all-remote company.

1. [GitLab's guide to starting a new remote role](/company/culture/all-remote/getting-started/)
1. [Adopting a self-service and self-learning mentality](/company/culture/all-remote/self-service/)
1. [Culture and experience as seen on the GitLab Blog](/blog/categories/culture/)
1. [GitLab's guide to making remote work](/company/culture/all-remote/)

### What our team members are saying

Interested in hearing about life at GitLab straight from our people? Feel free to read through the reviews on our [Glassdoor](https://www.glassdoor.com/Overview/Working-at-GitLab-EI_IE1296544.11,17.htm) and [Comparably](https://www.comparably.com/companies/gitlab) profiles.

The freedom to work from anywhere has impacted the lives of many of our team members. Here are some of [their stories](/company/culture/all-remote/stories/).

## GitLab’s outbound recruiting model

GitLab is embracing an innovative approach to the way we recruit new team members. This shift to an outbound recruiting model will allow us to continue to grow GitLab and build an even more diverse team in the most efficient way possible, while also sustaining our culture. Our Recruiting team will be focused on sourcing the most talented candidates for our open roles.

This model will allow us to focus even more during the interview process on whether candidates truly align with [our GitLab values](https://about.gitlab.com/handbook/values/). Our values [bind us as a company](https://about.gitlab.com/handbook/leadership/biggest-risks/#loss-of-the-values-that-bind-us) and make our culture unique. As we continue to grow, it’s more important than ever that future GitLab team members align to those values.

This model also supports [GitLab's vision for all-remote](https://about.gitlab.com/company/culture/all-remote/vision/): to spread opportunity to underserved locales, and to influence the increase of wages for remote work outside of metro areas.

We’ve seen promising results in the parts of the company where we’ve used this model so far, and we’re excited to have the capacity to iterate on this at a larger scale as we grow the GitLab team.

### Our hiring process and COVID-19

GitLab is an intentional company. This is seen in our approach to [onboarding](https://about.gitlab.com/handbook/general-onboarding/), [asynchronous work](https://about.gitlab.com/company/culture/all-remote/asynchronous/), [meetings](https://about.gitlab.com/company/culture/all-remote/meetings/), and [transparency](https://about.gitlab.com/handbook/values/#transparency).

We are closely monitoring the economic impact of COVID-19 on our business and working to take smart, proactive steps as we navigate through these uncertain times, including the acceleration of an ongoing project to be more intentional about recruiting and sourcing.

GitLab is also reprioritizing some of our open roles, and hiring for others at a later date so that our team is able to focus their efforts.

### How can I apply for a job?

This recruiting model means you’ll see some significant changes to the GitLab jobs page. ***We will no longer be accepting inbound applications for our roles***.

Instead, we invite you to [join the GitLab talent community](https://boards.greenhouse.io/gitlab/jobs/4700367002?gh_src=d865c64f2us). That’s where you’ll be able to share your resume with our team so that you can be considered for a career opportunity with GitLab.

### Where can I find a list of open roles?

The roles we’re actively sourcing for will be listed on our [careers page](https://about.gitlab.com/jobs/careers/), however there will not be a link to apply. To be considered for a career at GitLab, be sure to share your information with us by [joining our talent community](https://boards.greenhouse.io/gitlab/jobs/4700367002?gh_src=d865c64f2us).

### What if I already applied for a role at GitLab prior to these changes?

If you’ve already applied for one of the roles we’re prioritizing, your application will still be reviewed. Our team will provide you with feedback based on [the process outlined in our handbook](https://about.gitlab.com/handbook/hiring/interviewing/#rejecting-candidates).

## Country hiring guidelines

At GitLab, we hire people from all over the world and all walks of life.
Diversity & inclusion is one of our [core values](/handbook/values/).
However, as an [all-remote company](/company/culture/all-remote/) we do face challenges with hiring in certain countries.
Each country has unique and complex rules, laws, and regulations, that can affect our ability to conduct business, as well as the employability of the citizens and residents of those countries.

We are growing rapidly and continuously expanding our hiring capabilities in other geographies.
However, at this time we are unable to hire employees and contractors in the specified countries below:

<% Gitlab::Homepage::Jobs::HiringStatus.no_hiring_list.map {|no_hirable| no_hirable['country'] }.sort.each do |country_name| %>
- <%= country_name %>
<% end %>

We encourage you to continue to check our handbook as we establish our presence in other countries.

### Country payroll status

In countries listed in our [contract_factors.yml file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/contract_factors.yml), we have a payroll and can employ you as an employee. In all other countries, we can hire you as a contractor.

<a name="no-recruiters"></a>

## We don't accept solicitations by recruiters

At GitLab, we do not accept solicitations from recruiters, recruiting agencies, headhunters, or outsourcing organizations.
If you email us about this type of opportunity, [we'll reply](https://gitlab.com/gitlab-com/people-group/recruiting/blob/master/Templates%20for%20responding%20to%20third%20party%20agencies) with [a link to this paragraph](/jobs/faq/#no-recruiters) to indicate that we'd like to be removed from the contact list.

<div class="base-flex justify-center">
  <a class="btn cta-btn accent" href="/jobs/careers">Join our Talent Community</a>
</div>
