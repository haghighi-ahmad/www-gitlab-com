---
layout: handbook-page-toc
title: The Procurement Team
---

## On this page
{:.no_toc}

- TOC
{:toc}


Team members can purchase goods and services on behalf of the company in accordance with the [Signature Authorization Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/) and guide to [Spending Company Money](https://about.gitlab.com/handbook/spending-company-money/). However, all purchases being made on behalf of GitLab that are not a personal expense, must first be reviewed by procurement, then signed off by a member of the executive team. This ensures GitLab can appropriately plan for spend and assess vendor risk. 

## What if the Vendor I am working with doesn't require a contract?
1. Even if your vendor doesn't need a contract, a Vendor Contract Issue is still required under Requesting Procurement Services below.
1. The Vendor Contract approval process is designed to protect both you and the GitLab and the vendor. 
1. In this event we can and will provide terms to govern the transaction based on the level of risk.

## Prior to Contacting Procurement
Prior to engaging Procurement, please review the below guidelines:
1. Review the market capabilities defined by your overall spend *before* selecting your vendor.
1. Before sharing details and/or confidential information regarding our business needs, please obtain a [Mutual Non-Disclosure Agreement](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing) from the potential vendor(s). Refer to the [Signature Authorization Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/) for signing authority.
1. All vendors must adhere to the [GitLab Partner Code of Ethics](/handbook/people-group/code-of-conduct/#partner-code-of-ethics). Inform your vendor(s) it is mandatory they contractually adhere to this if they would like to do business with us. (Note these are typically not required in event related agreements unless the vendor is providing services).
1. Identify your bid requirements based on your estimated spend:
     >$0-$100K: No bid

     >$101K - $250: 2-3 Bids

     >Greater than $250K: RFP
1. Open a vendor contract approval issue based on the choices under Requesting Procurement Services. Do this BEFORE agreeing to any business and/or pricing terms.

## Requesting Procurement Services

Start working with the Procurement team by opening a vendor contract approval issue based on the type of purchase below. Procurement will not approve the request if the request is incomplete and/or missing information.

Contact Procurement directly in Slack via #procurement if you have any questions.

### 1: Purchase Type: Software or vendor that will process GitLab data

1. Open a Vendor Contract Approval issue with [this template](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts) to begin the process.
1. Create this issue BEFORE agreeing to business terms and/or pricing. 
1. It is preferred we negotiate the best pricing up front to keep our ongoing costs to a minimum across our long-term relationships with vendors.
1. If you have a field marketing or event contract where confidential data will be shared, use the above template.

A video tutorial of the issue creation process can be found [**HERE**](https://gitlab.zoom.us/rec/share/6-h-N5zR71tIS5Xdsn-Pf7NiD53Eeaa80XMZ-fUNxEfuCbA_5yfQNyOY4AUZsmwh?startTime=1584653128000)

### 2: Purchase Type: Existing vendor true-up/expansion/renewal

1. Open a Vendor Contract Approval issue with [this template](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts) **90-60 days before the existing contract expires**. 
1. Create this issue BEFORE agreeing to business terms and/or pricing. 
1. It is preferred we continue to evaluate supplier pricing at the time of renewal to keep our ongoing costs to a minimum across our long-term relationships with vendors. 
1. This can include addendums or PO adjustment that either do or do not change pricing.

A video tutorial of the issue creation process can be found [**HERE**](https://gitlab.zoom.us/rec/share/6-h-N5zR71tIS5Xdsn-Pf7NiD53Eeaa80XMZ-fUNxEfuCbA_5yfQNyOY4AUZsmwh?startTime=1584653128000)

### 3: Purchase Type: Field Marketing and Events withOUT Confidential Data

1. Open a Vendor Contract Marketing & Events Approval [issue with this template](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts_marketing_events)
1. Examples for this template type include marketing events, programs, sponsorships, catering, hotels, swag and services that do NOT involve the processing or sharing of data.
1. If you will be sharing confidential data with the vendor, please use the template under Purchase Type #1 above.

A video tutorial of the field marketing and events issue creation process can be found [**HERE**](https://gitlab.zoom.us/rec/share/vdJ0d5jS00RJTtKVxRj5fKkfMqXPaaa80XMY-fIFzx7GHZWxe_p688iPeZ_qU85O?startTime=1584654449000)


## Deep Dive on the Vendor Contract Issue Process
Procurement will not approve a contract issue until all other approvals have been received to validate the appropriate approval process and policies have been followed. In the event procurement approves an issue prior to other approvals in an attempt to avoid being the source of a backlog, procurement will comment in the issue that their approval is "subject to remaining approvals". At this point it is the responsibility of the issue owner to follow the remaining process to obtain contract signature.

>### Legal Engagement for Vendor Contracts

1. Legal is responsible for reviewing vendor contracts and will adhere to legal playbook.
1. A contract cannot be signed until it has been approved by the legal team. Once the legal team approves the contract, legal will upload the contract with the approval stamp. Contracts will not be signed unless the legal approval stamp is included.

>### Security Engagement for Vendor Contracts

1. Security is responsible for reviewing vendor security practicies and will adhere to the [Third Party Vendor Security Review Process](https://about.gitlab.com/handbook/engineering/security/third-party-vendor-security-review.html#email-template-for-requesting-security-documentation_). 
1. The Security Compliance team needs 3 business days to complete this review from the time they receive all necessary documentation from the vendor 
1. A contract cannot be signed until it has been approved by the security team. Once the security team approves the vendor and/or identifies gaps in the vendor's security practices for negotiation, security will provide their approval in the issue.
1. Consult the [Data Classification Policy](https://docs.google.com/document/d/15eNKGA3zyZazsJMldqTBFbYMnVUSQSpU14lo22JMZQY/edit#heading=h.a7l25bv5e2pi) to understand whether your contract will need security review. Any contracts that will share RED or ORANGE data will need security approval prior to signing. Additionally, an annual reassessment of vendor's security posture is performed as part of the contract renewal.
1. Complete a [Data Protection Impact Assessment](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/new?issuable_template=Data%20Protection%20Impact%20Assessment); please note that this will be done in partnership with GitLab's Data Protection Officer and reviewed by Security Compliance during the Security Review.

### Capacity & Back Log

In the event you have an issue that hasn't received a prompt response from procurement, requestor should tag Aleshia Hansen in the #procurement Slack channel and provide:

1. Link to the Vendor Contract Approval issue; and 
1. Timeline

In the event the procurement team is out of office (as highlighted in PTO calendar or Slack), and the matter is time sensitive, requestor should
contact #legal channel in Slack and provide:

1.  Link to Vendor Contract Approval Issue; and
2.  Reason for escalation, with timeline for requirement(s)

Legal will assign a team member to approve the procurement portion of the issue.

## Vendors & GitLab

### Vendor Onboarding

Vendors will be required to create an account within Tipalti in order to receive payment.

### Vendor Performance Issues

If there are performance and/or quality issues from an existing third-party vendor, procurement can support the resolution and/or re-evaluation of a replacement vendor.
Please log an issue and assign `a.hansen` for next steps.

## Related Docs and Templates

#### Documentation

* [Uploading Third Party Contracts to ContractWorks](/handbook/legal/vendor-contract-filing-process/)
* [Compliance](/handbook/legal/global-compliance/) - general information about compliance issues relevant to the company
* [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information) - general information about each legal entity of the company
* [Trademark](/handbook/marketing/corporate-marketing/#gitlab-trademark--logo-guidelines) - information regarding the usage of GitLab's trademark
* [Authorization Matrix](/handbook/finance/authorization-matrix/) - the authority matrix for spending and binding the company and the process for signing legal documents

##### Contract Templates

- [Mutual Non-Disclosure Agreement](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)

## Procurement KPI

Deliver quantified savings of > $3,000,000 over a rolling 12 month period.

Cost savings are achieved through the procure to pay process.
Savings are calculated as the savings achieved by comparing the initial vendor proposal price to the final purchase price.
In the event of a contract renewal, in addition to the above, savings can also be calculated as the savings achieved by comparing the previous cost per unit (eg. user, business metric, etc.) to the final cost per unit.
Savings negotiated at the cost per unit level are also cost savings to be calculated as savings.
Note these savings are not directly tied to budget.

Aligns with the following core business objectives:

* Control spend and build a culture of long-term savings on procurement costs.
* Streamline the purchasing process.
* Minimize financial risk.

## Procurement Main Objectives

Procurement operates with three main goals
1.  Cost Savings
2.  Centralize Spending
3.  Compliance