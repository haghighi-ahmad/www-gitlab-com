---
layout: handbook-page-toc
title: "Celebrations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Anniversary Celebrations

### Slack Announcements

Once per week, GitLab team-members with a hire-date anniversary will get a post in the `#team-member-updates` channel on Slack so all team members can help them celebrate.

### Anniversary Gifts

Directions on how to order your gift will be sent via email on the last day of the month of your anniversary. If you have not received your anniversary gift within 8 weeks (please consider global shipping) of your anniversary, please email peopleops@gitlab, so they can follow up.

Work Anniversary Gifts are as follows:
* Year 1:  Custom mug with a representation of your commits in GitLab
* Year 3:  GitLab Hoodie/Jacket 
* Year 5:  GitLab travel bag/ backpack
* Year 10: $500 Travel grant/ airbnb gift card

How to order anniversary gifts for People Experience Associates :
1. Create the minimum order. A minimum order consists of: 72 mugs, 10 jackets and 25 bags. This will be stored in the Supplier's warehouse and shipped when needed.
2. Complete the order [sheet](https://docs.google.com/spreadsheets/d/16DGY5F59yqbOvRYqpkulp55BGVmvuaEEM4GdGVJiMzo/edit#gid=1739552180) with all anniversaries from August 20th 2019. This order sheet is shared with our supplier in order for them to complete the shipping.
3. Email our supplier on the 1st day of every month.
4. People Experience Associates please refer to [Instructions on How to Send Anniversary Gifts](https://gitlab.com/gitlab-com/people-group/people-operations-and-experience-team-training/blob/master/Anniversary%20Gifts.md#instructions-on-how-to-send-anniversary-gifts) to follow when sending sending emails to team members.

## Birthdays

The company encourages all GitLab team-members to take a day of vacation on their birthday by utilizing our [paid time off](/handbook/paid-time-off/) policy.
**BirthdayBot** our official birthday bot will announce team members birthday on #celebrations channel for the rest of the company to send you their wishes. A few things to note;
1. Any team member wishing to be excluded should refrain from including their birthday on their Slack profile.
1. If you would like to be included but are not comfortable with inputing the year in Slack, you can use `0000` as the year in your slack profile (ex. 0000-12-29), which will just show the month and day in your Slack profile. Alternatively, just reply with day and month to the bot on a private/direct message under apps section in Slack.
1. The bot will not announce how old you are turning just that it's your birthday, feel free to share that information on the thread if you wish to.
