---
layout: markdown_page
title: "Category Direction - Accessibility Testing"
---

- TOC
{:toc}

## Accessibility Testing

Beyond being a compliance requirement in many cases, accessibility testing is the right thing to do for your users. Accessibility testing is similar to UAT or Usability (which we track [here](/direction/verify/usability_testing/)), but we see accessibility as a primary concern of its own.

Interested in joining the conversation for this category? Please join us in the issues where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AAccessibility%20Testing)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why

Up next is [gitlab#39425](https://gitlab.com/gitlab-org/gitlab/issues/39425) (Show a11y change in the Merge Request) which builds on the release of [Automated a11y scanning of Review Apps](#25566).  This issue helps developers with the problem of knowing how their change potential degraded the accessibility of the project by providing actionable information to resolve issues created in the Merge Request.

## Maturity Plan

This category is currently at the "Planned" maturity level, and
our next maturity target is Minimal (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- ~~[Automated a11y scanning of Review Apps](https://gitlab.com/gitlab-org/gitlab/issues/25566)~~ - Released in 12.8
- [Show a11y change in the Merge Request](https://gitlab.com/gitlab-org/gitlab/issues/39425)
- [Show a full a11y report in GitLab](https://gitlab.com/gitlab-org/gitlab/issues/36170)

We may find in research that only some of these issues are needed to move the vision for this category forward. The work to move the vision is captured and being tracked in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/2021).

## Competitive Landscape

Competitors in this space are not providing first-party accessibility testing platforms, but do integrate with pa11y or other tools to generate results via the CI/CD pipeline. Implementing [gitlab#25566](https://gitlab.com/gitlab-org/gitlab/issues/25566) (accessibility testing for review apps MVC) will help us meet (or in many cases exceed) competitor capability be embedding it into our review apps, a natural home for them.

## Top Customer Success/Sales Issue(s)

There are no top CS/Sales issues for this category yet.

## Top Customer Issue(s)

With the release of GitLab 12.8 the top customer item ([gitlab#25566](https://gitlab.com/gitlab-org/gitlab/issues/25566)) was addressed. We look forward to feedback on the issues being tracked in the current epic for the category and beyond from customers.

## Top Internal Customer Issue(s)

Now that Automatic a11y ([gitlab#25566](https://gitlab.com/gitlab-org/gitlab/issues/25566)) testing has been released we are working with internal customers to implement items in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/2021).

A related Epic is the GitLab accessibility epic that contains items about making GitLab itself more accessible: [gitlab-org#567](https://gitlab.com/groups/gitlab-org/-/epics/567).  Loveable for this category is defined as being able to use GitLab to detect all of those issues in an automated way and see that they are addressed when fixed.

## Top Vision Item(s)

Beyond the item being worked on next [gitlab#39425](https://gitlab.com/gitlab-org/gitlab/issues/39425) that will expand our out-of-the-box accessibility testing tracking how [accessibility is changing over time](https://gitlab.com/gitlab-org/gitlab/issues/36171) in a project and providing a [view of the full accessibility report](https://gitlab.com/gitlab-org/gitlab/issues/36170) are items that move us closer to the long term vision for this category.
